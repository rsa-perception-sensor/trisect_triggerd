// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

#include <thread>
#include <condition_variable>
#include <mutex>
#include <list>

#include "trisect_triggerd/gpio_trigger.h"
#include "trisect_triggerd/rpc_common.h"

namespace trisect_triggerd {

class TriggerThread {
 public:
    static const int32_t TriggerOnTime_ns = 1e6;

    TriggerThread();
    TriggerThread( const TriggerThread & ) = delete;

    ~TriggerThread();

    int32_t setPeriod(int32_t ns);
    int32_t getPeriod() const;

    int count(void) const { return _count; }

 private:
    void threadLoop(void);

    int _triggerTimer;
    int _untriggerTimer;
    mutable std::mutex _timerMutex;

    int _count;

    std::thread _thread;
    bool _done;

    struct Trigger {
        GpioTrigger trigger;
        bool enabled;
    };

    std::array< Trigger, (uint)trigger_line_t::NUM_TRIGGER_LINES > _triggers;
};


}
