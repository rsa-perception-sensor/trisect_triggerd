
# License

This code is released under the [BSD 3-clause license](LICENSE).

## License for IPC

This software uses a fork of the [IPC](https://github.com/amarburg/IPC) module, which is released under the [Mozilla Public 2.0 License](third_party/IPC/LICENSE).