// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

#include <unistd.h>
#include <linux/gpio.h>

#include <string>

namespace trisect_triggerd {

class GpioTrigger {
 public:
    static const GpioTrigger LeftCamera;
    static const GpioTrigger RightCamera;

    GpioTrigger(const std::string &device, int pin);
    ~GpioTrigger();

    void trigger();
    void untrigger();

 protected:
    bool configureGpio();

    // I'm not a huge fan of isOpen? status variables, should shift to factory?
    bool _is_open;

    std::string _gpioDevice;
    int _gpioFd;
    int _pin;

    struct gpiohandle_request _gpioHandle;
};

}

