// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

static const char SocketPath[] = "/tmp/triggerd";

enum class simple_server_function_t {
    set_period_ns=0,
    get_period_ns,

    unknown
};

enum class trigger_line_t {
    TRIGGER_LEFT = 0,
    TRIGGER_RIGHT = 1,

    NUM_TRIGGER_LINES = 2
};

