// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#pragma once

#include <clocale>
#include <iostream>

#include "rpc.hpp"

#include "trisect_triggerd/rpc_common.h"

namespace trisect_triggerd {

class TriggerdClient {
 public:
    TriggerdClient( const std::string &path = SocketPath );

    int32_t get_period_ns();
    int32_t set_period_ns(int32_t ns);

 protected:
    std::string _socket_path;

};

}  // namespace trisect_triggerd
