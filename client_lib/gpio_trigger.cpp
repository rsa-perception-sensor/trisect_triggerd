// Released under BSD 3-Clause license.  See LICENSE.
//
// Copyright (c) 2021, University of Washington
// All rights reserved.

#include <string.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>

#include <iostream>
#include <string>

#include "trisect_triggerd/gpio_trigger.h"

namespace trisect_triggerd {

const Gpio Gpio::LeftCamera("/dev/gpiochip0", 124);
const Gpio Gpio::RightCamera("/dev/gpiochip1", 19);


Gpio::Gpio(const std::string &device, int pin)
    : _gpioDevice(device),
    _gpioFd(-1),
    _pin(pin) {
    _is_open = configureGpio();
}

Gpio::~Gpio() {
    if (_gpioHandle.fd >= 0) close( _gpioHandle.fd );
    if (_gpioFd > 0) close( _gpioFd );
}

bool Gpio::configureGpio() {
    struct stat st;

    if (-1 == stat(_gpioDevice.c_str(), &st)) {
        std::cerr << "Cannot identify '" << _gpioDevice << "': " << errno
                  << ", " << strerror(errno) << std::endl;
        return false;
    }

    if (!S_ISCHR(st.st_mode)) {
        std::cerr << _gpioDevice << " is not a char device" << std::endl;
        return false;
    }

    _gpioFd = open(_gpioDevice.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (-1 == _gpioFd) {
        std::cerr << "Cannot open '" << _gpioDevice << "': " << errno
                  << ", " << strerror(errno) << std::endl;
        return false;
    }


    // struct gpiochip_info info;
    // int  ret;
    
    // // Query GPIO chip information
    // ret = ioctl(_gpioFd, GPIO_GET_CHIPINFO_IOCTL, &info);
    // if (ret == -1) {
    //         printf("Unable to get chip info from ioctl: %s", strerror(errno));
    //         return;
    // }
    // fprintf(stdout,"Chip name: %s\n", info.name);
    // fprintf(stdout,"Chip label: %s\n", info.label);
    // fprintf(stdout,"Number of lines: %d\n", info.lines);

    //struct gpioline_info line_info;
    //line_info.line_offset = _pin;
    // ret = ioctl(_gpioFd, GPIO_GET_LINEINFO_IOCTL, &line_info);
    // if (ret == -1)
    // {
    //     printf("Unable to get line info from offset %d: %s", _pin, strerror(errno));
    // }
    // else
    // {
    //     printf("offset: %d, name: %s, consumer: %s. Flags:\t[%s]\t[%s]\t[%s]\t[%s]\t[%s]\n",
    //         _pin,
    //         line_info.name,
    //         line_info.consumer,
    //         (line_info.flags & GPIOLINE_FLAG_IS_OUT) ? "OUTPUT" : "INPUT",
    //         (line_info.flags & GPIOLINE_FLAG_ACTIVE_LOW) ? "ACTIVE_LOW" : "ACTIVE_HIGHT",
    //         (line_info.flags & GPIOLINE_FLAG_OPEN_DRAIN) ? "OPEN_DRAIN" : "...",
    //         (line_info.flags & GPIOLINE_FLAG_OPEN_SOURCE) ? "OPENSOURCE" : "...",
    //         (line_info.flags & GPIOLINE_FLAG_KERNEL) ? "KERNEL" : "");
    // }

    _gpioHandle.lineoffsets[0] = _pin;
    _gpioHandle.lines = 1;
    _gpioHandle.flags = GPIOHANDLE_REQUEST_OUTPUT;

    int ret = ioctl(_gpioFd, GPIO_GET_LINEHANDLE_IOCTL, &_gpioHandle);
    if (ret == -1) {
        std::cerr << "Unable to get line handle from ioctl: " << strerror(errno) << std::endl;
        return false;
    }

    return true;
}

void Gpio::trigger() {
    if( !_is_open ) return;

    struct gpiohandle_data data;
    data.values[0] = 1;
    // if( _flash ) std::cerr << _flash;
    auto ret = ioctl(_gpioHandle.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
}

void Gpio::untrigger() {
    if( !_is_open ) return;

    struct gpiohandle_data data;
    data.values[0] = 0;
    auto ret = ioctl(_gpioHandle.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
}

}  // namespace trisect_triggerd
